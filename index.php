<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
    html, body { height: 100%; margin: 0; padding: 0; }
        #map { height: 400px; }
    </style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

</head>
<body>
    <div class="container">
        <div class="page-header" id="banner">
            <h1>mAPI</h1>
        </div>

        <div class="row">
            <a href="trajet.html" class="btn btn-primary btn-sm" role="button">Trajet</a>
            <a href="#" id="geoloc" class="btn btn-info btn-sm" role="button">Géolocalisation</a>
            <a href="#" id="marker" class="btn btn-primary btn-sm" role="button">Info bulle</a>
            <a href="#" id="altitude" class="btn btn-info btn-sm" role="button">Altitude</a>
            <a href="#" id="pos" class="btn btn-primary btn-sm" role="button">Position</a>
            <a href="#" id="population" class="btn btn-info btn-sm" role="button">Population</a>
            <a href="#" id="traffic" class="btn btn-primary btn-sm" role="button">Traffic</a>
            <a href="#" id="streetview" class="btn btn-info btn-sm" role="button">Street view</a>
            <br>&nbsp; <!-- non breakable space pour séparer les boutons et la maps -->
            <div id="map"></div>
            <div id="distance"></div>
            <div id="duree"></div>
            <div id="adresse"></div>
        </div>

    </div>

    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCw4UvSDWn6E84yOgt7ZrjQiAxYbmipNq4&callback=initMap">
    </script>

    <script src="js/mapi-app.js"></script>

    <script>
        $( document ).ready(function() {



    <?php
if(isset($_GET['mode'])) {
    switch ($_GET['mode']) {
        case "1":
            ?>
            toggleStreetView();
            <?php
            break;
        case "2":
            ?>
            elevation();
            <?php
            break;
        case "3":
            ?>
            marqueur();
            <?php
            break;
        case "4":
            ?>
            geoloc();
            <?php
            break;
        case "5":
            ?>
            populationVilles();
            <?php
            break;
    }
}
?>

});
</script>

</body>
</html>