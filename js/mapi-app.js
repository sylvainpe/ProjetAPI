// Script Sylvain

$('#streetview').click(function () {
    toggleStreetView();
});

$('#altitude').click(function () {
    elevation();
});

$('#marker').click(function () {
    marqueur();
});

$('#geoloc').click(function () {
    geoloc();
});

$('#population').click(function () {
    populationVilles();
});

$('#pos').click(function () {
    position();
});


$('#traffic').click(function() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: {lat: 43.130390, lng: 5.978801}
    });

    var trafficLayer = new google.maps.TrafficLayer();
    trafficLayer.setMap(map);
});


function geoloc() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 16
    });
    var infoWindow = new google.maps.InfoWindow({map: map});

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Vous êtes ici !!!');
            map.setCenter(pos);
        });
    } else {
        console.log("pas de géolocalisation")
    }

}


var panorama;
var isen = {lat: 43.120673, lng: 5.939488};
var restau = {lat: 43.150776, lng: 5.930707};
var centreBulle = {lat: 43.137788, lng: 5.934858};
// Callback function

function elevation() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: {lat: 43.147233, lng: 5.945548},
        mapTypeId: 'terrain'
    });
    var elevator = new google.maps.ElevationService;
    var infowindow = new google.maps.InfoWindow({map: map});

    // Add a listener for the click event. Display the elevation for the LatLng of
    // the click inside the infowindow.
    map.addListener('click', function (event) {
        displayLocationElevation(event.latLng, elevator, infowindow);
    });
}

function displayLocationElevation(location, elevator, infowindow) {
    // Initiate the location request
    elevator.getElevationForLocations({
        'locations': [location]
    }, function (results, status) {
        infowindow.setPosition(location);
        if (status === google.maps.ElevationStatus.OK) {
            // Retrieve the first result
            if (results[0]) {
                // Open the infowindow indicating the elevation at the clicked position.
                infowindow.setContent('L\'altitude au point cliqué <br>est de ' +
                    results[0].elevation + ' mètres.');
            } else {
                infowindow.setContent('Aucun résultat trouvé');
            }
        } else {
            infowindow.setContent('Elevation service failed due to: ' + status);
        }
    });
}

function marqueur() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: centreBulle
    });

    var ISENDescription = '<div id="content">' +
        '<div id="siteNotice">' +
        '</div>' +
        '<h1 id="firstHeading" class="firstHeading">ISEN Toulon</h1>' +
        '<div id="bodyContent">' +
        '<p><b>L\'ISEN Toulon</b> est une école d\'ingénieurs. Voilà.</p>' +
        '</div>' +
        '</div>';

    var restauDescription = '<div id="content">' +
        '<div id="siteNotice">' +
        '</div>' +
        '<h1 id="firstHeading" class="firstHeading">Le Drap d\'Or</h1>' +
        '<div id="bodyContent">' +
        '<p><b>Le Drap d\'Or</b> est un restaurant. Voilà.</p>' +
        '</div>' +
        '</div>';



    var infowindow = new google.maps.InfoWindow({
        content: ISENDescription
    });
    var infowindow1 = new google.maps.InfoWindow({
        content: restauDescription
    });

    var marker = new google.maps.Marker({
        position: isen,
        map: map,
        title: 'ISEN Toulon'
    });
    var marker1 = new google.maps.Marker({
        position: restau,
        map: map,
        title: 'Le Drap d\'Or'
    });
    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });
    marker1.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png')
    marker1.addListener('click', function () {
        infowindow1.open(map, marker1);
    });
}

function populationVilles() {

    var citymap = {
        toulon: {
            center: {lat: 43.1222, lng: 5.93},
            population: 165584
        },
        laseyne: {
            center: {lat: 43.1, lng: 5.883333},
            population: 64675
        },
        valette: {
            center: {lat: 43.138333, lng: 5.983056},
            population: 22271
        }
    };

    // Create the map.
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: {lat: 43.1222, lng: 5.93},
        mapTypeId: google.maps.MapTypeId.TERRAIN
    });

    // Construct the circle for each value in citymap.
    // Note: We scale the area of the circle based on the population.
    for (var city in citymap) {
        // Add the circle for this city to the map.
        new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: citymap[city].center,
            radius: Math.sqrt(citymap[city].population) * 10
        });
    }
}

function initMap() {

    // Set up the map
    var map = new google.maps.Map(document.getElementById('map'), {
        center: isen,
        zoom: 18,
        streetViewControl: false
    });

    // Set up the markers on the map
    var cafeMarker = new google.maps.Marker({
        position: {lat: 40.730031, lng: -73.991428},
        map: map,
        icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_icon&chld=cafe|FFFF00',
        title: 'Cafe'
    });

    var bankMarker = new google.maps.Marker({
        position: {lat: 40.729681, lng: -73.991138},
        map: map,
        icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_icon&chld=dollar|FFFF00',
        title: 'Bank'
    });

    var busMarker = new google.maps.Marker({
        position: {lat: 40.729559, lng: -73.990741},
        map: map,
        icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_icon&chld=bus|FFFF00',
        title: 'Bus Stop'
    });

    // We get the map's default panorama and set up some defaults.
    // Note that we don't yet set it visible.
    panorama = map.getStreetView();
    panorama.setPosition(isen);
    panorama.setPov(/** @type {google.maps.StreetViewPov} */({
        heading: 265,
        pitch: 0
    }));
}


function toggleStreetView() {
    var toggle = panorama.getVisible();
    if (toggle == false) {
        panorama.setVisible(true);
    } else {
        panorama.setVisible(false);
    }
}

function position() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10
    });
    var infoWindow = new google.maps.InfoWindow({}); //fenetre qui s'ouvre quand on clique sur un marker

    var marker;
    var pos;
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;

    directionsDisplay.setMap(map);

    function placeMarker(location) { //fonction pour placer un marker
        var clickLatlng = location;
        if (marker == undefined) { //si le marker n'existe pas
            marker = new google.maps.Marker({
                position: location,
                map: map,
                draggable: false, //pour savoir si le marker est deplacable en maintenant le clique de la souris
                animation: google.maps.Animation.DROP
            });
        }
        else { //si le marker existe deja, on ne veux surtout pas en recreer un autre, sinon on pourrait en avoir 50 000
            marker.setPosition(location);
        }

        var totalDistance = 0;
        var totalDuration = 0;
        directionsService.route({
            origin: pos, // Notre position
            destination: location,
            travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                var legs = response.routes[0].legs;
                for(var i=0; i<legs.length; ++i) {
                    totalDistance += legs[i].distance.value;
                    totalDuration += legs[i].duration.value;
                }
                console.log("dist : "+totalDistance + " metres");
                console.log("duree : "+totalDuration + " secondes");
                document.getElementById('distance').innerHTML = "Trajet de "+totalDistance/1000+" kilometres.";
                document.getElementById('duree').innerHTML = "Trajet de "+totalDuration/60+" minutes, soit "+totalDuration/3600+" heures.";

            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });

        var geocoderClick = new google.maps.Geocoder();
        geocoderClick.geocode({ 'location': clickLatlng }, function (results, status) { //on recupere un JSON (results) de cet endroit
            var adresse = ""; //on defini des champs à completer
            var ville = "";
            var pays = "";

            for (var i = 0; i < results[0].address_components.length; i++) { //on regarde les elements du JSON
                if (results[0].address_components[i].types[0] == "street_number") { //on cherche un element nomme "street_number"
                    adresse += results[0].address_components[i].long_name + " "; //l'API propose un "long_name" et un "short_name"
                }
                if (results[0].address_components[i].types[0] == "route") { //on cherche un element nomme "route"
                    adresse += results[0].address_components[i].long_name + " ";
                }
                if (results[0].address_components[i].types[0] == "locality") { //on cherche un element nomme "locality"
                    ville += results[0].address_components[i].long_name;
                }
                if (results[0].address_components[i].types[0] == "country") { //on cherche un element nomme "country"
                    pays += results[0].address_components[i].long_name;
                }
            }
            console.log(adresse + ", " + ville + ", " + pays);
            document.getElementById('adresse').innerHTML ="<strong>Adresse du clic</strong> : " + adresse + ", " + ville + ", " + pays;
        });



        google.maps.event.addListener(marker, 'click', function () { //si on clique sur un marker d'un evenement
            infoWindow.setContent("Vous avez cliquer ici !!!"); //on est redirige vers sa page de detail
            infoWindow.open(map, marker); //ouverture de l'infoWindow
        });
    }

    google.maps.event.addListener(map, 'click', function (event) { //lorsqu'on clique sur la map
        var clickLatLng = event.latLng; //on recupere les coordonnees gps du clique
        placeMarker(clickLatLng); //on y place un marker
    });

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setCenter(pos);

            var myPosition = new google.maps.Marker({
                map: map,
                icon: "img/here.png",
                animation: google.maps.Animation.DROP, //animation du marker qui "tombe du ciel"
                position: pos
            });

            google.maps.event.addListener(myPosition, 'click', function () { //lorsqu'on clique sur le marker de notre position
                infoWindow.setContent('Vous etes ICI !!!'); //message de l'infoWindow
                infoWindow.open(map, myPosition); //l'infoWindow s'ouvre sur la map, et au dessus du marker "myPosition"
            });
        });
    } else {console.log("pas de géolocalisation");
    }
}