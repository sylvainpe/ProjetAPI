function initMap() {

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10
    });
    var infoWindow = new google.maps.InfoWindow({}); //fenetre qui s'ouvre quand on clique sur un marker

    var marker;
    var pos;
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;

    var geocoderClick = new google.maps.Geocoder();

    directionsDisplay.setMap(map);

    function placeMarker(location) { //fonction pour placer un marker
        var clickLatlng = location;
        if (marker == undefined) { //si le marker n'existe pas
            marker = new google.maps.Marker({
                position: location,
                map: map,
                draggable: true, //pour savoir si le marker est deplacable en maintenant le clique de la souris
                animation: google.maps.Animation.DROP
            });
        }
        else { //si le marker existe deja, on ne veux surtout pas en recreer un autre, sinon on pourrait en avoir 50 000
            marker.setPosition(location);
        }

        var totalDistance = 0;
        var totalDuration = 0;
        directionsService.route({
            origin: pos,
            destination: location,
            travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                var legs = response.routes[0].legs;
                for(var i=0; i<legs.length; ++i) {
                    totalDistance += legs[i].distance.value;
                    totalDuration += legs[i].duration.value;
                }
                console.log("dist : "+totalDistance + " metres");
                console.log("duree : "+totalDuration + " secondes");
                document.getElementById('distance').innerHTML = "Trajet de "+totalDistance/1000+" kilometres.";
                document.getElementById('duree').innerHTML = "Trajet de "+totalDuration/60+" minutes, soit "+totalDuration/3600+" heures.";

            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });

        var geocoderClick = new google.maps.Geocoder();
        geocoderClick.geocode({ 'location': clickLatlng }, function (results, status) { //on recupere un JSON (results) de cet endroit
            var adresse = ""; //on defini des champs à completer
            var ville = "";
            var pays = "";

            for (var i = 0; i < results[0].address_components.length; i++) { //on regarde les elements du JSON
                if (results[0].address_components[i].types[0] == "street_number") { //on cherche un element nomme "street_number"
                    adresse += results[0].address_components[i].long_name + " "; //l'API propose un "long_name" et un "short_name"
                }
                if (results[0].address_components[i].types[0] == "route") { //on cherche un element nomme "route"
                    adresse += results[0].address_components[i].long_name + " ";
                }
                if (results[0].address_components[i].types[0] == "locality") { //on cherche un element nomme "locality"
                    ville += results[0].address_components[i].long_name;
                }
                if (results[0].address_components[i].types[0] == "country") { //on cherche un element nomme "country"
                    pays += results[0].address_components[i].long_name;
                }
            }
            console.log(adresse + ", " + ville + ", " + pays);
            document.getElementById('adresse').innerHTML ="<u>Adresse du clique :</u> " + adresse + ", " + ville + ", " + pays;
        });



        google.maps.event.addListener(marker, 'click', function () { //si on clique sur un marker d'un evenement
            infoWindow.setContent("Vous avez cliquer ici !!!"); //on est redirige vers sa page de detail
            infoWindow.open(map, marker); //ouverture de l'infoWindow
        });
    }

    google.maps.event.addListener(map, 'click', function (event) { //lorsqu'on clique sur la map
        var clickLatLng = event.latLng; //on recupere les coordonnees gps du clique
        placeMarker(clickLatLng); //on y place un marker
    });

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setCenter(pos);

            var myPosition = new google.maps.Marker({
                map: map,
                icon: "img/here.png",
                animation: google.maps.Animation.DROP, //animation du marker qui "tombe du ciel"
                position: pos
            });

            google.maps.event.addListener(myPosition, 'click', function () { //lorsqu'on clique sur le marker de notre position
                infoWindow.setContent('Vous etes ICI !!!'); //message de l'infoWindow
                infoWindow.open(map, myPosition); //l'infoWindow s'ouvre sur la map, et au dessus du marker "myPosition"
            });
        });
    } else {console.log("pas de déolocalisation");
    }




}